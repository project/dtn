<?php

/**
 * @file
 * The admin settings for the DTN module.
 *
 * Handles the admin form for the DTN module.
 */

/**
 * Function that returns the settings form for the DTN module.
 */
function dtn_configuration_settings() {

  // Get the DTN username.
  $form['dtn_news']['dtn_username'] = array(
    '#title' => t('DTN username'),
    '#type' => 'textfield',
    '#default_value' => variable_get('dtn_username', ''),
    '#required' => TRUE,
  );

  // Get the DTN password.
  $form['dtn_news']['dtn_password'] = array(
    '#title' => t('DTN password'),
    '#type' => 'textfield',
    '#default_value' => variable_get('dtn_password', ''),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
