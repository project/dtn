<?php

/**
 * @file
 * Admin functions for the DTN weather module.
 */

/**
 * DTN futures main settings form.
 */
function dtn_futures_configuration_settings($form, &$form_state) {

  $form['dtn_futures_test_server'] = array(
    '#title' => t('Use the DTN test server'),
    '#description' => t('Uncheck this when you are ready to take your data live.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('dtn_futures_test_server', 1),
  );

  return system_settings_form($form);
}

/**
 * DTN futures block settings form.
 */
function dtn_futures_block_configuration_settings($form, &$form_state) {

  for ($i = 1; $i <= 50; $i++) {
    $available_blocks[$i] = $i;
  }

  $form['dtn_futures_available_blocks'] = array(
    '#title' => t('Futures blocks'),
    '#description' => t('How many futures blocks would you like to be available.'),
    '#type' => 'select',
    '#options' => $available_blocks,
    '#default_value' => variable_get('dtn_futures_available_blocks', 1),
  );

  return system_settings_form($form);
}
