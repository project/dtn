<?php

/**
 * @file
 * The admin settings for the DTN module.
 *
 * Handles the admin form for the DTN module.
 */

/**
 * Function that returns the settings form for the DTN module.
 */
function dtn_configuration_settings() {

  // Set our default node types.
  $node_types_options = array('' => t('Please select a node type.'));

  // Loop through our node types to fill our options array.
  foreach (node_type_get_types() as $type => $details) {
    $node_types_options[$type] = $details->name;
  }

  $cron_interval = drupal_map_assoc(array(
    4,
    6,
    8,
    10,
    12,
    14,
    16,
    18,
    20,
    22,
    24,
  ));

  // Default field options for mapping the body field.
  $field_options = array();

  // Get a list of available fields for the node type.
  $fields = field_info_instances('node', variable_get('dtn_news_node_type', ''));

  // If fields are available, setup our field options.
  if (count($fields)) {
    foreach ($fields as $field_name => $field) {
      $field_options[$field_name] = $field['label'];
    }
  }
  else {
    $field_options[-1] = t('Choose a node type to get field options.');
  }

  // Get available text formats.
  $formats = filter_formats();

  $format_options = array();

  foreach ($formats as $key => $format) {
    $format_options[$key] = $format->name;
  }

  $form['dtn_news'] = array(
    '#type' => 'fieldset',
    '#title' => t('DTN News Feeds'),
  );

  // Check whether to use the test or live server.
  $form['dtn_news']['dtn_news_enabled'] = array(
    '#title' => t('Enable the DTN news aggregator'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('dtn_news_enabled', FALSE),
  );

  // Check whether to use the test or live server.
  $form['dtn_news']['dtn_news_server'] = array(
    '#title' => t('DTN server'),
    '#type' => 'select',
    '#options' => drupal_map_assoc(array('test', 'live')),
    '#default_value' => variable_get('dtn_news_server', 'test'),
    '#required' => TRUE,
  );

  // Get the DTN service.
  $form['dtn_news']['dtn_news_service'] = array(
    '#title' => t('DTN service'),
    '#type' => 'textfield',
    '#default_value' => variable_get('dtn_news_service', ''),
    '#required' => TRUE,
  );

  // Get the DTN feed type.
  $form['dtn_news']['dtn_news_feed_type'] = array(
    '#title' => t('DTN feed type'),
    '#type' => 'select',
    '#options' => drupal_map_assoc(array('headlines', 'all')),
    '#default_value' => variable_get('dtn_news_feed_type', ''),
    '#required' => TRUE,
  );

  // Get the node types we would like these news stories saved as.
  $form['dtn_news']['dtn_news_node_type'] = array(
    '#title' => t('DTN node type'),
    '#type' => 'select',
    '#options' => $node_types_options,
    '#default_value' => variable_get('dtn_news_node_type', ''),
    '#required' => TRUE,
  );

  // Get the node's fields to map the story to.
  $form['dtn_news']['dtn_news_field_type'] = array(
    '#title' => t('DTN body field'),
    '#type' => 'select',
    '#options' => $field_options,
    '#default_value' => variable_get('dtn_news_field_type', ''),
    '#required' => TRUE,
  );

  // Get the format options.
  $form['dtn_news']['dtn_news_field_format'] = array(
    '#title' => t('DTN body field'),
    '#type' => 'select',
    '#options' => $format_options,
    '#default_value' => variable_get('dtn_news_field_format', 'plain_text'),
    '#required' => TRUE,
  );

  // Get the cron interval.
  $form['dtn_news']['dtn_news_cron_interval'] = array(
    '#title' => t('DTN refresh interval'),
    '#type' => 'select',
    '#options' => $cron_interval,
    '#default_value' => variable_get('dtn_news_cron_interval', 24),
    '#required' => TRUE,
    '#description' => t('Hours to wait for refresh.'),
  );

  $dtn_logos = [
    'full-color.png' => t('Full logo'),
    'full-bw.png' => t('Full logo, black and white'),
    'small-bw.png' => t('Small logo, black and white'),
    'small-color.png' => t('Small logo, color.'),
    'none' => t('No logo (this goes against DTN requirements)'),
  ];

  $logo_table_options = [
    'header' => [],
    'rows' => [
      0 => [],
    ],
  ];


  foreach ($dtn_logos as $file => $name) {
    if ($file == 'none') {
      continue;
    }

    array_push($logo_table_options['header'], $name);

    $cell = theme('image', [
      'path' => drupal_get_path('module', 'dtn_news') . '/images/' . $file,
      'alt' => $name,
      'attributes' => array(
        'style' => 'max-width:100px;height:auto;',
      ),
    ]);

    array_push($logo_table_options['rows'][0], $cell);

  }

  // Get the cron interval.
  $form['dtn_news']['dtn_news_logo'] = array(
    '#title' => t('DTN news logo'),
    '#type' => 'select',
    '#options' => $dtn_logos,
    '#default_value' => variable_get('dtn_news_logo', ''),
    '#required' => TRUE,
    '#description' => t('What logo should be placed at the end of DTN aggregated stories.'),
    '#suffix' => theme('table', $logo_table_options),
  );

  return system_settings_form($form);
}
