<?php

/**
 * @file
 * Custom code for the DTN Weather module.
 */

/**
 * Implements hook_menu().
 */
function dtn_weather_menu() {

  $items['admin/config/services/dtn/weather'] = array(
    'title' => 'DTN weather settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('dtn_weather_configuration_settings'),
    'access arguments' => array('administer dtn weather'),
    'description' => 'Configure DTN settings.',
    'file' => 'includes/dtn_weather.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );

  $items['admin/config/services/dtn/weather/settings'] = array(
    'title' => 'DTN weather settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('dtn_weather_configuration_settings'),
    'access arguments' => array('administer dtn weather'),
    'description' => 'Configure DTN settings.',
    'file' => 'includes/dtn_weather.admin.inc',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 0,
  );

  $items['admin/config/services/dtn/weather/blocks'] = array(
    'title' => 'DTN weather blocks',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('dtn_weather_block_configuration_settings'),
    'access arguments' => array('administer dtn weather'),
    'description' => 'Add weather blocks to Drupal.',
    'file' => 'includes/dtn_weather.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  );

  $items['dtn-weather/%'] = array(
    'title' => 'Current weather conditions',
    'page callback' => 'dtn_weather',
    'page arguments' => array('current', 1),
    'access arguments' => array('view dtn weather'),
    'description' => 'Show weather conditions',
    'file' => 'includes/dtn_weather.weather.inc',
    'type' => MENU_NORMAL_ITEM,
    'weight' => 0,
  );

  $items['dtn-weather/%/current'] = array(
    'title' => 'Current weather conditions',
    'page callback' => 'dtn_weather',
    'page arguments' => array('current', 1),
    'access arguments' => array('view dtn weather'),
    'description' => 'Show weather conditions',
    'file' => 'includes/dtn_weather.weather.inc',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 1,
  );

  $items['dtn-weather/%/hourly-forecast'] = array(
    'title' => 'Hourly forecast',
    'page callback' => 'dtn_weather',
    'page arguments' => array('hourly', 1),
    'access arguments' => array('view dtn weather'),
    'description' => 'Hourly forecast',
    'file' => 'includes/dtn_weather.weather.inc',
    'type' => MENU_CALLBACK,
    'weight' => 2,
  );

  $items['dtn-weather/%/5-day-forecast'] = array(
    'title' => '5 day forecast',
    'page callback' => 'dtn_weather',
    'page arguments' => array('5-day', 1),
    'access arguments' => array('view dtn weather'),
    'description' => '5 day forecast',
    'file' => 'includes/dtn_weather.weather.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 3,
  );

  $items['dtn-weather/%/10-day-forecast'] = array(
    'title' => '10 day forecast',
    'page callback' => 'dtn_weather',
    'page arguments' => array('10-day', 1),
    'access arguments' => array('view dtn weather'),
    'description' => '10 day forecast',
    'file' => 'includes/dtn_weather.weather.inc',
    'type' => MENU_CALLBACK,
    'weight' => 4,
  );

  $items['dtn-weather/%/radar-map'] = array(
    'title' => 'Radar map',
    'page callback' => 'dtn_weather',
    'page arguments' => array('radar-map', 1),
    'access arguments' => array('view dtn weather'),
    'description' => 'Radar map',
    'file' => 'includes/dtn_weather.weather.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 5,
  );

  $items['dtn-weather/callback/%/%'] = array(
    'title' => 'Ajax weather callback',
    'page callback' => 'dtn_weather_ajax_callback',
    'page arguments' => array(2, 3),
    'access arguments' => array('view dtn weather'),
    'file' => 'includes/dtn_weather.weather.inc',
    'type' => MENU_CALLBACK,
    'weight' => 6,

  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function dtn_weather_permission() {
  return array(
    'administer dtn weather' => array(
      'title' => t('Administer DTN weather settings'),
      'description' => t('Administer DTN weather settings'),
    ),
    'view dtn weather' => array(
      'title' => t('View DTN weather content'),
      'description' => t('Allows users to view DTN weather content.'),
    ),
  );
}

/**
 * Implements hook_theme().
 */
function dtn_weather_theme() {
  return array(
    'dtn_weather_current' => array(
      'variables' => array(
        'observations' => array(),
        'data' => array(),
      ),
      'template' => 'templates/dtn-weather-current',
      'pattern' => '__',
    ),

    'dtn_weather_observation' => array(
      'variables' => array(
        'observation' => array(),
        'data' => array(),
      ),
      'template' => 'templates/dtn-weather-observation',
      'pattern' => '__',
    ),

    'dtn_weather_observation_forecast' => array(
      'variables' => array(
        'observation' => array(),
        'data' => array(),
      ),
      'template' => 'templates/dtn-weather-observation-forecast',
      'pattern' => '__',
    ),

    'dtn_weather_forecast' => array(
      'variables' => array(
        'observations' => array(),
        'heading' => '',
        'data' => array(),
      ),
      'template' => 'templates/dtn-weather-forecast',
      'pattern' => '__',
    ),

    'dtn_weather_map' => array(
      'variables' => array(
        'map' => array(),
        'heading' => '',
      ),
      'template' => 'templates/dtn-weather-map',
      'pattern' => '__',
    ),
  );
}

/**
 * API Function to get a token from DTN servers.
 */
function dtn_weather_get_token() {

  // If the token is not expired, return the cached one.
  if (variable_get('dtn_weather_token_expires', '') > REQUEST_TIME) {
    return variable_get('dtn_weather_token', '');
  }
  else {
    // Call the getToken service to get a new token.
    $result = dtn_weather_call_service('getToken', FALSE);

    $token = (string) $result->response->AccountToken->Token;
    $expires = strtotime($result->response->AccountToken->attributes()->expires[0]);

    variable_set('dtn_weather_token_expires', $expires);
    variable_set('dtn_weather_token', $token);

    return $token;
  }
}

/**
 * Call the DTN service.
 */
function dtn_weather_call_service($service, $token = TRUE, $additional_fields = array()) {

  if (variable_get('dtn_weather_test_server', TRUE)) {
    $url = 'http://catapi.aghost.net/api/weather/';
  }
  else {
    $url = 'http://api.aghost.net/api/weather/';
  }

  if ($token) {
    $fields = array(
      'token' => urlencode(dtn_weather_get_token()),
      'method' => urlencode($service),
    );
  }
  else {
    $fields = array(
      'method' => urlencode($service),
      'username' => urlencode(variable_get('dtn_username', '')),
      'password' => urlencode(variable_get('dtn_password', '')),
    );
  }

  // Add any additional fields needed.
  $fields = array_merge($fields, $additional_fields);

  $fields_string = '';

  // Setup our parameters for CURL.
  foreach ($fields as $key => $value) {
    $fields_string .= $key . '=' . $value . '&';
  }

  rtrim($fields_string, '&');

  // Open connection.
  $ch = curl_init();

  // Set the url, number of POST vars, POST data.
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_POST, count($fields));
  curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

  // Execute post.
  $result = curl_exec($ch);

  // Close connection.
  curl_close($ch);

  // Remove the tns: that is added to the DTN results.
  $result = str_replace('tns:', '', $result);
  $result = str_replace('@', '', $result);

  // Make sure we have valid XML.
  if ($xml = simplexml_load_string($result)) {
    if ($xml->response->ServiceError) {
      drupal_set_message(t('An error occurred while access the DTN service. Please try again later.'), 'error');
      watchdog('DTN Weather', 'The following error occurred while accessing DTN: %error', array('%error' => $xml->response->ServiceError), WATCHDOG_ERROR);
    }
    else {
      return $xml;
    }
  }
  else {
    drupal_set_message(t('Unable to access DTN. Please try again later.'), 'error');
  }
}

/**
 * Call the DTN map service.
 */
function dtn_weather_call_map_service($token = TRUE, $additional_fields = array()) {

  if (variable_get('dtn_weather_test_server', TRUE)) {
    $url = 'http://catagwx.dtn.com/RegionalRadar.cfm';
  }
  else {
    $url = 'http://agwx.dtn.com/RegionalRadar.cfm';
  }

  if (!isset($additional_fields['zip']) && isset($additional_fields['lat']) && isset($additional_fields['lon'])) {
    $additional_fields['ll'] = $additional_fields['lat'] . ',' . $additional_fields['lon'];

    unset($additional_fields['lat']);
    unset($additional_fields['lon']);

    $cache_region = drupal_clean_css_identifier($additional_fields['ll']);
  }
  else {
    $cache_region = drupal_clean_css_identifier($additional_fields['zip']);
  }

  $fields = array(
    'key' => urlencode(dtn_weather_get_token()),
  );

  // Add any additional fields needed.
  $fields = array_merge($fields, $additional_fields);

  $fields_string = '';

  // Setup our parameters for CURL.
  foreach ($fields as $key => $value) {
    $fields_string .= $key . '=' . $value . '&';
  }

  rtrim($fields_string, '&');

  // Open connection.
  $ch = curl_init();

  // Set the url, number of POST vars, POST data.
  curl_setopt($ch, CURLOPT_URL, $url . '?' . $fields_string);
  curl_setopt($ch, CURLOPT_POST, count($fields));
  curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

  // Execute post.
  $result = curl_exec($ch);

  // Close connection.
  curl_close($ch);

  if ($result) {
    if ($additional_fields['animate']) {
      $filepath = 'dtn_weather_map_' . $cache_region . '_level-' . $additional_fields['level'] . '_width-' . $additional_fields['width'] . '_height-' . $additional_fields['height'] . '_animate-' . $additional_fields['animate'] . '.gif';
    }
    else {
      $filepath = 'dtn_weather_map_' . $cache_region . '_level-' . $additional_fields['level'] . '_width-' . $additional_fields['width'] . '_height-' . $additional_fields['height'] . '_animate-' . $additional_fields['animate'] . '.png';
    }

    $dtn_tmp_directory = variable_get('dtn_weather_file_directory', 'public://dtn-weather');

    if (file_prepare_directory($dtn_tmp_directory, FILE_CREATE_DIRECTORY)) {
      $file_temp = file_save_data($result, 'public://dtn-weather', FILE_EXISTS_REPLACE);

      return $file_temp;
    }
  }
  else {
    drupal_set_message(t('Unable to access DTN mapping service. Please try again later.'), 'error');
  }

}

/**
 * Function to call the DTN weather service.
 */
function dtn_weather_get_results($service, $region) {

  // Get the server to pull content from.
  if (variable_get('dtn_weather_server', 'test') == 'live') {
    $service_url = 'http://api.aghost.net/api/weather/';
    $radar_url = 'http://agwx.dtn.com/RegionalRadar.cfm';
  }
  else {
    $service_url = 'http://catapi.aghost.net/api/weather/';
    $radar_url = 'http://catagwx.dtn.com/RegionalRadar.cfm';
  }

  // @todo - get our token and cache it as a variable.

  switch ($service) {

    case 'token':

      $url = $service_url;

      $fields = array(
        'method' => urlencode('getToken'),
        'username' => urlencode('e034990102'),
        'password' => urlencode('koEniG@53b'),
      );

      break;

    case 'current':

      $url = $service_url;

      $fields = array(
        'method' => urlencode('GetStationWeather'),
        'hrlyobs' => 24,
        'zip' => $region,
        'token' => urlencode($token),
      );
      break;
  }

  $fields_string = '';

  // Setup our URL for the CURL post.
  foreach ($fields as $key => $value) {
    $fields_string .= $key . '=' . $value . '&';
  }

  rtrim($fields_string, '&');

  // Open our CURL connection.
  $ch = curl_init();

  // Set the url, number of POST vars, POST data.
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_POST, count($fields));
  curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

  // Execute post.
  $result = curl_exec($ch);

  // Close connection.
  curl_close($ch);

  // Make sure we have valide XML.
  if ($xml = simplexml_load_string($result)) {
    if ($xml->NEWSQUERY_ROW->STORYID == -1) {
      watchdog('DTN', 'Error accessing the DTN feed. Error message was: !error', array('!error' => $xml->NEWSQUERY_ROW->TITLE), WATCHDOG_ERROR);
      return FALSE;
    }
    else {
      return $xml;
    }
  }

  return FALSE;
}

/**
 * Return the appropriate units.
 */
function dtn_weather_units($type) {
  $units = variable_get('dtn_weather_unitsystem', 'US');

  $return = FALSE;

  switch ($units) {
    case 'US':
      switch ($type) {
        case 'temperature':
          $return = t('F');
          break;

        case 'precipitation':
        case 'snow depth':
          $return = t('in');

          break;

        case 'wind':
          $return = t('mph');

          break;

        case 'pressure':
          $return = t('inHg');

          break;
      }
      break;

    case 'UK':
      switch ($type) {
        case 'temperature':
          $return = t('C');

          break;

        case 'precipitation':
          $return = t('mm');

          break;

        case 'snow depth':
          $return = t('cm');

          break;

        case 'wind':
          $return = t('mph');

          break;

        case 'pressure':
          $return = t('mb');

          break;
      }

      break;

    default:
      switch ($type) {
        case 'temperature':
          $return = t('C');

          break;

        case 'precipitation':
          $return = t('mm');

          break;

        case 'snow depth':
          $return = t('cm');

          break;

        case 'wind':
          $return = t('km/h');

          break;

        case 'pressure':
          $return = t('mb');

          break;
      }
      break;
  }

  return $return;
}

/**
 * Implements hook_block_info().
 */
function dtn_weather_block_info() {
  // Loop through the number of blocks the user wanted and make them available.
  for ($i = 1; $i <= variable_get('dtn_weather_available_blocks', 1); $i++) {
    $blocks['dtn_weather_block_' . $i] = array(
      'info' => t('DTN Weather block !var (!region)', array(
        '!var' => $i,
        '!region' => variable_get('dtn_weather_block_' . $i . '_postal_code', 'NO REGION SET'),
      )
      ),
    );
  }

  return $blocks;
}

/**
 * Implements hook_block_configure().
 */
function dtn_weather_block_configure($delta) {
  if (substr($delta, 0, 17) == 'dtn_weather_block') {
    $form = array();

    $form['dtn_weather_postal_code'] = array(
      '#type' => 'textfield',
      '#title' => t('Postal code'),
      '#description' => t('Please enter the postal code that you would like this block to display weather information for.'),
      '#default_value' => variable_get($delta . '_postal_code', ''),
    );

    $form['dtn_weather_type'] = array(
      '#type' => 'select',
      '#title' => t('Type'),
      '#options' => array(
        'current' => t('Current conditions'),
        '5-day' => t('5 day forecast'),
        'radar-map' => t('Radar map'),
        'user_form' => t('User postal code form'),
        'user_location' => t('Use the location of the user'),
      ),
      '#description' => t('Choose the content this block will display.'),
      '#default_value' => variable_get($delta . '_type', ''),
    );

    return $form;
  }
}

/**
 * Implements hook_block_save().
 */
function dtn_weather_block_save($delta, $edit) {
  if (substr($delta, 0, 17) == 'dtn_weather_block') {
    variable_set($delta . '_postal_code', $edit['dtn_weather_postal_code']);
    variable_set($delta . '_type', $edit['dtn_weather_type']);
  }
}

/**
 * Implements hook_block_view().
 */
function dtn_weather_block_view($delta) {
  module_load_include('inc', 'dtn_weather', 'includes/dtn_weather.weather');

  // If the block is setup to display a form, lets load our postal code form.
  if (variable_get($delta . '_type', '') == 'user_form') {
    if (isset($_COOKIE[$delta])) {
      $block['subject'] = t('Weather conditions for %zip', array('%zip' => $_COOKIE[$delta]));
    }
    else {
      $block['subject'] = t('Get weather conditions for your area.');
    }

    $block['content'] = drupal_get_form('dtn_weather_postal_code_select_form', $delta);
  }
  elseif (variable_get($delta . '_type', '') == 'user_location') {
    // Try and use the user's location.
    drupal_add_js(drupal_get_path('module', 'dtn_weather') . '/js/dtn_weather_location.js');

    $block['content'] = '<div class="dtn-weather-user-location"></div>';
  }
  else {
    $block['subject'] = t('Weather conditions for %zip', array('%zip' => variable_get($delta . '_postal_code', '')));
    $block['content'] = dtn_weather(variable_get($delta . '_type', ''), variable_get($delta . '_postal_code', ''), $delta);
  }

  return $block;
}

/**
 * Implements hook_forms().
 */
function dtn_weather_forms($form_id, $args) {
  $forms = array();

  if (strpos($form_id, 'dtn_weather_postal_code_select_form_') !== FALSE) {
    $forms[$form_id] = array(
      'callback' => 'dtn_weather_postal_code_select_form',
    );
  }

  return $forms;
}
