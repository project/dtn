<?php

/**
 * @file
 * Admin functions for the DTN weather module.
 */

/**
 * Configuration settings form.
 */
function dtn_weather_configuration_settings() {

  $form = array();

  $form['dtn_weather_test_server'] = array(
    '#title' => t('Use the DTN test server'),
    '#description' => t('Uncheck this when you are ready to take your data live.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('dtn_weather_test_server', TRUE),
  );

  $form['dtn_weather_unitsystem'] = array(
    '#title' => t('Unit system'),
    '#type' => 'select',
    '#options' => drupal_map_assoc(array('US', 'Metric', 'UK')),
    '#default_value' => variable_get('dtn_weather_unitsystem', 'US'),
  );

  return system_settings_form($form);
}

/**
 * Block configuration settings form.
 */
function dtn_weather_block_configuration_settings() {

  $form = array();

  for ($i = 1; $i <= 50; $i++) {
    $available_blocks[$i] = $i;
  }

  $form['dtn_weather_available_blocks'] = array(
    '#title' => t('Weather blocks'),
    '#description' => t('How many weather blocks would you like to be available.'),
    '#type' => 'select',
    '#options' => $available_blocks,
    '#default_value' => variable_get('dtn_weather_available_blocks', 1),
  );

  return system_settings_form($form);
}
