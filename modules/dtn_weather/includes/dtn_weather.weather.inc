<?php

/**
 * @file
 * Included functions for the DTN weather module.
 */

/**
 * Function to display weather conditions.
 */
function dtn_weather($tab, $region = FALSE, $block = '', $more_link = FALSE, $lat = FALSE, $long = FALSE) {
  if ($region) {
    $latlon = explode(' ', $region);
    if (count($latlon) == 2) {
      $region = FALSE;
      $lat = $latlon[0];
      $long = $latlon[1];
      $cache_region = drupal_clean_css_identifier($lat . $long);
    }
    else {
      $cache_region = drupal_clean_css_identifier($region);
    }
  }

  switch ($tab) {

    case 'radar-map':
      $additional_fields = array(
        'level' => 3,
        'animate' => FALSE,
        'width' => 640,
        'height' => 480,
      );

      if ($region) {
        $additional_fields['zip'] = urlencode(check_plain($region));
      }

      if ($lat && $long && !$region) {
        $additional_fields['lat'] = $lat;
        $additional_fields['lon'] = $long;
      }

      // Check if the map image was cached.
      $content = cache_get('dtn_weather_' . $cache_region . '_radar_map_' . $block);

      // If we have a cached map and the expire time hasn't passed,
      // use the cache.
      if ($content && $content->expire >= REQUEST_TIME) {
        return $content->data;
      }
      else {
        $results = dtn_weather_call_map_service(TRUE, $additional_fields);

        $image = theme('image', array('path' => $results->uri));

        $heading = t('Radar map for @zip. Updated !time', array('@zip' => $region, '!time' => format_date($results->timestamp)));

        $content = theme(array('dtn_weather_map__' . $region, 'dtn_weather_map'), array('map' => $image, 'heading' => $heading));

        // Save our images to the cache for 10 minutes.
        // @todo: Lets make the cache timeout a variable.
        cache_set('dtn_weather_' . $cache_region . '_radar_map_' . $block, $content, 'cache', REQUEST_TIME + 600);

        return $content;
      }

      break;

    case '5-day':
    case '10-day':

      if ($tab == '5-day') {
        $forecast = 5;
      }
      elseif ($tab == '10-day') {
        $forecast = 10;
      }

      // Check if we have forecast data cached.
      $content = cache_get('dtn_weather_forecast_' . $forecast . '_' . $cache_region . '_' . $block);

      // If we have cached the forecast weather data and
      // the expire time hasn't passed, use the cache.
      if ($content && $content->expire >= REQUEST_TIME) {
        return $content->data;
      }
      else {
        $additional_fields = array(
          'dailyObs' => $forecast,
          'dailyFc' => $forecast,
        );

        if ($region) {
          $additional_fields['zip'] = urlencode(check_plain($region));
        }

        if ($lat && $long && !$region) {
          $additional_fields['lat'] = $lat;
          $additional_fields['lon'] = $long;
        }

        $results = dtn_weather_call_service('GetStationWeather', TRUE, $additional_fields);

        $station = array(
          'identifier' => filter_xss((string) $results->response->StationWeather->Station->Identifier),
          'latitude' => (float) $results->response->StationWeather->Station->Latitude,
          'longitude' => (float) $results->response->StationWeather->Station->Longitude,
          'name' => filter_xss((string) $results->response->StationWeather->Station->Name),
          'minorfipsalphaCode' => filter_xss((string) $results->response->StationWeather->Station->MinorFIPSAlphaCode),
          'majorfipsalphaCode' => filter_xss((string) $results->response->StationWeather->Station->MajorFIPSAlphaCode),
          'gmtoffset' => (float) $results->response->StationWeather->Station->GMTOffset,
          'observerdst' => (float) $results->response->StationWeather->Station->ObserveDST,
        );

        foreach ($results->response->StationWeather->DailyForecastData->DailyForecast as $day => $value) {
          $observation = array(
            'timestamp' => strtotime((string) $value->ValidDateRange->StartDateTime),
            'conditions' => array(
              'textdescription' => filter_xss((string) $value->Conditions->TextDescription),
              'image' => '//agwx.dtn.com/wxconditions/2/2/' . (string) $value->Conditions->IconFilename . '.png',
            ),
            'temperature' => array(
              'temp' => '',
              'high' => (float) $value->MaximumTemperature,
              'low' => (float) $value->MinimumTemperature,
              'unit' => dtn_weather_units('temperature'),
            ),
            'feelsliketemperature' => array(
              'temp' => '',
              'high' => (float) $value->FeelsLikeMaximumTemperature,
              'low' => (float) $value->FeelsLikeMinimumTemperature,
              'unit' => dtn_weather_units('temperature'),
            ),
            'precipitation' => array(
              'rain' => (float) $value->Precipitation->ProbabilityOfPrecipitation,
              'snow' => (float) $value->Precipitation->ProbabilityOfSnow,
              'ice' => (float) $value->Precipitation->ProbabilityOfIce,
              'mix' => (float) $value->Precipitation->ProbabilityOfMix,
              'thunderstorm' => (float) $value->Precipitation->ProbabilityOfThunderstorm,
              'amount' => (float) $value->Precipitation->AmountLiquid,
              'unit' => dtn_weather_units('precipitation'),
            ),
            'dewpoint' => (float) $value->DewPoint,
            'relativehumidity' => (float) $value->RelativeHumidity,
            'wind' => array(
              'direction' => array(
                'abbreviation' => '',
                'heading' => '',
              ),
              'sustainedspeed' => (float) $value->Wind->SustainedSpeed,
              'unit' => dtn_weather_units('wind'),
            ),
          );

          $data['observations'][] = theme(array('dtn_weather_observation_forecast__' . $region, 'dtn_weather_observation_forecast'), array(
            'observation' => dtn_weather_build_forecast_observation($station, $observation, $day),
            'heading' => '',
          ));
        }

        $heading = t('Forecast for !station_name, !station_state', array('!station_name' => $station['name'], '!station_state' => $station['minorfipsalphaCode']));

        $content = theme(array('dtn_weather_forecast__' . $region, 'dtn_weather_forecast'), array(
          'observations' => $data['observations'],
          'heading' => $heading,
          'data' => $data,
        ));

        // Save our weather information to the cache with a 10 minute timeout.
        // @todo: Lets make the cache timeout a variable.
        cache_set('dtn_weather_forecast_' . $forecast . '_' . $cache_region . '_' . $block, $content, 'cache', REQUEST_TIME + 600);

        return $content;
      }

      break;

    default:
      // Check if the map image was cached.
      $content = cache_get('dtn_weather_current_' . $cache_region . '_' . $block);

      // If we have cached the current weather and
      // the expire time hasn't passed, use the cache.
      if ($content && $content->expire >= REQUEST_TIME) {
        return $content->data;
      }
      else {
        // Default is the 'current' weather.
        $additional_fields = array(
          'hrlyobs' => 1,
        );

        if ($region) {
          $additional_fields['zip'] = urlencode(check_plain($region));
        }

        if ($lat && $long && !$region) {
          $additional_fields['lat'] = $lat;
          $additional_fields['lon'] = $long;
        }

        $results = dtn_weather_call_service('GetStationWeather', TRUE, $additional_fields);

        $station = array(
          'identifier' => filter_xss((string) $results->response->StationWeather->Station->Identifier),
          'latitude' => (float) $results->response->StationWeather->Station->Latitude,
          'longitude' => (float) $results->response->StationWeather->Station->Longitude,
          'name' => filter_xss((string) $results->response->StationWeather->Station->Name),
          'minorfipsalphaCode' => filter_xss((string) $results->response->StationWeather->Station->MinorFIPSAlphaCode),
          'majorfipsalphaCode' => filter_xss((string) $results->response->StationWeather->Station->MajorFIPSAlphaCode),
          'gmtoffset' => (float) $results->response->StationWeather->Station->GMTOffset,
          'observerdst' => (float) $results->response->StationWeather->Station->ObserveDST,
          'region' => urlencode(check_plain($region)),
        );

        $observation = array(
          'timestamp' => strtotime((string) $results->response->StationWeather->HourlyObservationData->HourlyObservation->ValidDateTime),
          'conditions' => array(
            'skycover' => filter_xss((string) $results->response->StationWeather->HourlyObservationData->HourlyObservation->Conditions->SkyCover),
            'textdescription' => filter_xss((string) $results->response->StationWeather->HourlyObservationData->HourlyObservation->Conditions->TextDescription),
            'image' => '//agwx.dtn.com/wxconditions/2/2/' . (string) $results->response->StationWeather->HourlyObservationData->HourlyObservation->Conditions->IconFilename . '.png',
          ),
          'temperature' => array(
            'temp' => (float) $results->response->StationWeather->HourlyObservationData->HourlyObservation->Temperature,
            'unit' => dtn_weather_units('temperature'),
          ),
          'feelsliketemperature' => array(
            'temp' => (float) $results->response->StationWeather->HourlyObservationData->HourlyObservation->FeelsLikeTemperature,
            'unit' => dtn_weather_units('temperature'),
          ),
          'dewpoint' => (float) $results->response->StationWeather->HourlyObservationData->HourlyObservation->DewPoint,
          'relativehumidity' => (float) $results->response->StationWeather->HourlyObservationData->HourlyObservation->RelativeHumidity,
          'wind' => array(
            'direction' => array(
              'abbreviation' => filter_xss((string) $results->response->StationWeather->HourlyObservationData->HourlyObservation->Wind->Direction->Abbreviation),
              'heading' => (float) $results->response->StationWeather->HourlyObservationData->HourlyObservation->Wind->Direction->Heading,
            ),
            'sustainspeed' => (float) $results->response->StationWeather->HourlyObservationData->HourlyObservation->Wind->SustainSpeed,
            'unit' => dtn_weather_units('wind'),
          ),
          'atmosphericpressure' => array(
            'pressure' => (float) $results->response->StationWeather->HourlyObservationData->HourlyObservation->AtmosphericPressure,
            'unit' => dtn_weather_units('pressure'),
          ),
          'soiltemperature' => array(
            'temp' => (float) $results->response->StationWeather->HourlyObservationData->HourlyObservation->SoilTemperature,
            'unit' => dtn_weather_units('temperature'),
          ),
        );

        $data['station'] = $station;
        $data['observations'][] = theme(array('dtn_weather_observation__' . $region, 'dtn_weather_observation'), dtn_weather_build_observation($station, $observation, $more_link), $data);
        $data['observation_data'] = $observation;

        $content = theme(array('dtn_weather_current__' . $region, 'dtn_weather_current'), array(
          'observations' => $data['observations'],
          'data' => $data,
        ));

        // Save our weather information to the cache with a 10 minute timeout.
        // @todo: Lets make the cache timeout a variable.
        cache_set('dtn_weather_current_' . $cache_region, $content . '_' . $block, 'cache', REQUEST_TIME + 600);

        return $content;
      }

      break;
  }

}

/**
 * Helper function to build the theme data for observations.
 */
function dtn_weather_build_observation($station, $observation, $more_link = FALSE) {
  $data['heading'] = t('Current conditions in !station_name, !station_state', array('!station_name' => $station['name'], '!station_state' => $station['minorfipsalphaCode']));

  $data['image'] = theme('image', array(
    'path' => $observation['conditions']['image'],
    'alt' => $observation['conditions']['textdescription'],
    'title' => $observation['conditions']['textdescription'],
  ));

  $data['temperature'] = t('Current temperature: !temp !unit', array('!temp' => $observation['temperature']['temp'], '!unit' => $observation['temperature']['unit']));
  $data['feels_like_temperature'] = t('Feels like: !temp !unit', array('!temp' => $observation['feelsliketemperature']['temp'], '!unit' => $observation['feelsliketemperature']['unit']));;
  $data['description'] = t('!desc with !sky skies', array('!desc' => $observation['conditions']['textdescription'], '!sky' => $observation['conditions']['skycover']));
  $data['updated'] = t("Updated on !date", array('!date' => format_date($observation['timestamp'])));
  $data['wind'] = t('Wind from the !direction at !speed !unit', array(
    '!direction' => $observation['wind']['direction']['abbreviation'],
    '!speed' => $observation['wind']['sustainspeed'],
    '!unit' => $observation['wind']['unit'],
  ));
  $data['humidity'] = t('Relative Humidity: !humidity', array('!humidity' => $observation['relativehumidity']));
  $data['dew_point'] = t('Dew point:  !dewpoint', array('!dewpoint' => $observation['dewpoint']));
  $data['atmospheric_pressure'] = t('Atmospheric pressure: !pressure !unit', array('!pressure' => $observation['atmosphericpressure']['pressure'], '!unit' => $observation['atmosphericpressure']['unit']));
  $data['soil_temperature'] = t('Soil temperature: !temp !unit', array('!temp' => $observation['soiltemperature']['temp'], '!unit' => $observation['soiltemperature']['unit']));;

  if ($more_link) {
    if (!empty($station['region'])) {
      $data['more_link'] = l(t('View more details'), 'dtn-weather/' . $station['region']);
    }
    elseif (!empty($station['latitude']) && !empty($station['longitude'])) {
      $data['more_link'] = l(t('View more details'), 'dtn-weather/' . $station['latitude'] . ' ' . $station['longitude']);
    }

  }

  return $data;
}

/**
 * Helper function to build the theme data of forecast observations.
 */
function dtn_weather_build_forecast_observation($station, $observation, $day = FALSE) {
  $data['heading'] = t('!date', array(
    '!station_name' => $station['name'],
    '!station_state' => $station['minorfipsalphaCode'],
    '!date' => format_date($observation['timestamp']),
  ));

  $data['image'] = theme('image', array(
    'path' => $observation['conditions']['image'],
    'alt' => $observation['conditions']['textdescription'],
    'title' => $observation['conditions']['textdescription'],
  ));

  $data['high_temperature'] = t('High temperature: !temp !unit', array('!temp' => $observation['temperature']['high'], '!unit' => $observation['temperature']['unit']));
  $data['low_temperature'] = t('Low temperature: !temp !unit', array('!temp' => $observation['temperature']['low'], '!unit' => $observation['temperature']['unit']));
  $data['description'] = t('!desc', array('!desc' => $observation['conditions']['textdescription']));
  $data['rain'] = t('Probability of precipitation: !precip%', array('!precip' => $observation['precipitation']['rain']));
  $data['thunderstorm'] = t('Probability of thunderstorms: !precip%', array('!precip' => $observation['precipitation']['thunderstorm']));

  // Determine if we should show the snow/ice fields or not.
  if (($observation['temperature']['low'] < 40 && $observation['temperature']['unit'] == 'F') || ($observation['temperature']['low'] < 10 && $observation['temperature']['unit'] == 'C')) {
    $data['snow'] = t('Probability of snow: !precip%', array('!precip' => $observation['precipitation']['snow']));
    $data['ice'] = t('Probability of ice: !precip%', array('!precip' => $observation['precipitation']['ice']));
    $data['mix'] = t('Probability of wintry mix: !precip%', array('!precip' => $observation['precipitation']['mix']));
  }

  $data['wind'] = t('Wind speed: !speed !unit', array('!speed' => $observation['wind']['sustainedspeed'], '!unit' => $observation['wind']['unit']));
  $data['humidity'] = t('Relative Humidity: !humidity', array('!humidity' => $observation['relativehumidity']));
  $data['dew_point'] = t('Dew point:  !dewpoint', array('!dewpoint' => $observation['dewpoint']));

  return $data;
}

/**
 * Custom block form to allow the user to chooose their location.
 */
function dtn_weather_postal_code_select_form($form, &$form_state, $delta) {
  $form = array();

  $form['dtn_weather_delta'] = array(
    '#type' => 'value',
    '#value' => $delta,
  );

  // If the user already entered their location, save the weather
  // in a markup field and allow the user to change their location.
  if (isset($_COOKIE[$delta])) {

    $form['dtn_weather_markup'] = array(
      '#type' => 'markup',
      '#markup' => dtn_weather('current', $_COOKIE[$delta], $delta, TRUE),
    );

    $form['dtn_weather_postal_code'] = array(
      '#type' => 'value',
      '#value' => $_COOKIE[$delta],
    );

    $form['dtn_weather_postal_code_cancel'] = array(
      '#type' => 'submit',
      '#default_value' => t('Change this location'),
    );
  }
  else {
    $form['dtn_weather_postal_code'] = array(
      '#type' => 'textfield',
      '#title' => t('Postal code'),
      '#required' => TRUE,
      '#size' => 10,
      '#maxlength' => 7,
      '#description' => t('Please enter the postal code that you would like this block to display weather information for.'),
      '#default_value' => variable_get($delta . '_postal_code', ''),
    );

    $form['dtn_weather_postal_code_submit'] = array(
      '#type' => 'submit',
      '#default_value' => t('Submit'),
    );

  }

  $form['#submit'][] = 'dtn_weather_postal_code_select_form_submit';

  return $form;
}

/**
 * Submit handling function for the postal code select form.
 */
function dtn_weather_postal_code_select_form_submit($form, &$form_state) {
  // If the user chooses to change their location.
  // Delete the cookie and return.
  if (isset($form_state['values']['dtn_weather_postal_code_cancel'])) {
    setcookie($form_state['values']['dtn_weather_delta'], '', REQUEST_TIME - 3600, '/');
  }
  else {
    // Let's set a cookie for this block.
    setcookie($form_state['values']['dtn_weather_delta'], $form_state['values']['dtn_weather_postal_code'], REQUEST_TIME + 5184000, '/');
  }

  // Clear the cache for this block if the form is submitted.
  cache_clear_all('dtn_weather_current_' . $form_state['values']['dtn_weather_postal_code'] . '_' . $form_state['values']['dtn_weather_delta'], 'cache');
}

/**
 * Custom ajax callback for the user location block.
 */
function dtn_weather_ajax_callback($lat, $long) {
  $content = dtn_weather('default', FALSE, '', TRUE, $lat, $long);
  print $content;
  exit();

}
