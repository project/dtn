(function ($) {

  Drupal.behaviors.dtn_weather_location = {
    attach: function(context, settings) {

      settings['dtnWeatherElements'] = [];

      jQuery.extend(Drupal.settings,  settings);

      $('.dtn-weather-user-location').once('dtn-weather-user-location', function(){
        // If the user allows their location, use it.
        if (navigator.geolocation) {
          Drupal.settings.dtnWeatherElements.push($(this));
          navigator.geolocation.getCurrentPosition(Drupal.dtnWeather.getUserWeather, Drupal.dtnWeather.getUserWeatherError)
        }
        else {
          // If the user doesn't allow location, Let them know.
          $(this).html(Drupal.t("We are unable to determine your location."));
        }
      });
    }
  };
  
  // Defined our variable for the functions below.
  Drupal.dtnWeather = {};
  
  /**
   * Get the user's location and return the weather accordingly.
   */
  Drupal.dtnWeather.getUserWeather = function(position) {
    var element = Drupal.settings.dtnWeatherElements.shift();

    $.ajax({
      url: Drupal.settings.basePath + "?q=dtn-weather/callback/" + position.coords.latitude + "/" + position.coords.longitude,
      type: 'GET',
      success: function(response) {
        element.html(response);
      }
    });
  }
  
  /**
   * Error handling for the getCurrentPosition function call.
   */
  Drupal.dtnWeather.getUserWeatherError = function(error) {
    var element = Drupal.settings.dtnWeatherElements.shift();
    
    switch(error.code) {
      case error.PERMISSION_DENIED:
        element.html(Drupal.t("You must allow your location to view the current weather."));
        break;
      case error.POSITION_UNAVAILABLE:
        element.html(Drupal.t("We are unable to determine your location."));
        break;
      case error.TIMEOUT:
        element.html(Drupal.t("The request for your location has taken too long."));
        break;
      case error.UNKNOWN_ERROR:
        element.html(Drupal.t("An unknown error occurred."));
        break;
    }

  }

})(jQuery);