<?php

/**
 * @file
 * Template file for dtn current weather observations.
 */
?>
<div class="dtn-current-weather-observations">  
  <?php
      foreach ($observations as $observation) {
        print ($observation);
      }
  ?>
</div>
