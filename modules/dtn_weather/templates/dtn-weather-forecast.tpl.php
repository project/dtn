<?php

/**
 * @file
 * Template file for dtn current weather forecasts.
 */
?>
<div class="dtn-weather forecast">
  <h2 class="dtn-weather-forecast-heading"><?php print $heading ?></h2>  
 <?php
      foreach ($observations as $observation) {
        print ($observation);
      }
  ?>
  
</div>
