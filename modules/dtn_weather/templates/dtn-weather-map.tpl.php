<?php

/**
 * @file
 * Template file for dtn current weather observations.
 */
?>

<div class="dtn-weather-map">
  
  <h2 class="dtn-weather-map-heading"><?php print $heading ?></h2>
  
  <div class="dtn-weather-map-image"><?php print $map ?></div>
  
</div>
