<?php

/**
 * @file
 * Template file for dtn current weather observations.
 */
?>

<div class="dtn-weather-observation dtn-weather-observation-forecast">
  
  <h2 class="dtn-observation-heading"><?php print $observation['heading'] ?></h2>
  
  <div class="dtn-observation-image"><?php print $observation['image'] ?></div>
  
  <div class="dtn-observation-description">
    <?php print $observation['description'] ?>
  </div>
 
  <div class="dtn-observation-high-temperature">
    <?php print $observation['high_temperature'] ?>
  </div>
  
  <div class="dtn-observation-low-temperature">
    <?php print $observation['low_temperature'] ?>
  </div>
 
  <div class="dtn-observation-rain">
    <?php print $observation['rain'] ?>
  </div>
  
  <div class="dtn-observation-thunderstorm">
    <?php print $observation['thunderstorm'] ?>
  </div>
  
  <div class="dtn-observation-wind">
    <?php print $observation['wind'] ?>
  </div>
  
  <div class="dtn-observation-humidity">
    <?php print $observation['humidity'] ?>
  </div>
  
  <div class="dtn-observation-dew-point">
    <?php print $observation['dew_point'] ?>
  </div>
  
</div>
