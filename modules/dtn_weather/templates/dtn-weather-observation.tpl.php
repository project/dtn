<?php

/**
 * @file
 * Template file for dtn current weather observations.
 */
?>

<div class="dtn-weather-observation">
  
  <h2 class="dtn-observation-heading"><?php print $heading ?></h2>
  
  <div class="dtn-observation-image"><?php print $image ?></div>
 
  <div class="dtn-observation-temperature">
    <?php print $temperature ?>
  </div>
 
  <div class="dtn-observation-feels-like-temperature">
    <?php print $feels_like_temperature ?>
  </div>
  
  <div class="dtn-observation-description">
    <?php print $description ?>
  </div>
  
  <div class="dtn-observation-timestamp">
    <?php print $updated; ?>
  </div>
  
  <div class="dtn-observation-wind">
    <?php print $wind ?>
  </div>
  
  <div class="dtn-observation-humidity">
    <?php print $humidity ?>
  </div>
  
  <div class="dtn-observation-dew-point">
    <?php print $dew_point ?>
  </div>
  
  <div class="dtn-observation-atmospheric-pressure">
    <?php print $atmospheric_pressure ?>
  </div>
  
  <div class="dtn-observation-soil-temperature">
    <?php print $soil_temperature ?>
  </div>
  
  <div class="dtn-observation-more">
    <?php print (isset($more_link) ? $more_link : '') ?>
  </div>
</div>
